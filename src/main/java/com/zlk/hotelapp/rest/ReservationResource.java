package com.zlk.hotelapp.rest;


import com.zlk.hotelapp.response.ReservationResponse;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping(ResourceConstants.ROOM_RESERVATION_V1)
public class ReservationResource {

    @RequestMapping(path = "" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ReservationResponse> getAvailableRooms(
            @RequestParam(value = "chekin")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
            LocalDate chekin ,
            @RequestParam(value = "chekout")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
            LocalDate chekout){
        return new ResponseEntity<>(new ReservationResponse() , HttpStatus.OK );
    }
}
