package com.zlk.hotelapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javax.persistence.Entity;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
public class HotelappApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelappApplication.class, args);
	}
}
